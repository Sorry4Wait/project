<?php

namespace app\controllers;


use app\models\UploadForm;
use Yii;
use yii\web\Controller;
use yii\web\UploadedFile;

class ImageController extends Controller
{
    public function actionIndex()
    {
        $images = UploadForm::find()->all();
        $model = new UploadForm();
        if (Yii::$app->request->isPost) {
            $model->file = UploadedFile::getInstance($model, 'file');

            if ($model->validate()) {
                $model->filename = $model->file->baseName . '.' . $model->file->extension;
                $model->save();
                $model->file->saveAs(Yii::getAlias('@webroot').'/upload/' . $model->file->baseName . '.' . $model->file->extension);
                return $this->actionAlbum();
            }
        }
        return $this->render('index',[
            'model' => $model,
            'images' => $images
        ]);
    }

    public function actionAlbum()
    {
        $images = UploadForm::find()->all();
        return $this->render('album',[
            'images' => $images
        ]);
    }
    public function actionDelete($id)
    {
        $model = UploadForm::findOne(['id' => $id]);
        $model->delete();

        return $this->redirect(['album']);
    }




}