<?php

use yii\db\Migration;

/**
 * Class m181031_112826_create_table_upload_image
 */
class m181031_112826_create_table_upload_image extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('upload_image', [
            'id' => $this->primaryKey(),
            'filename' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m181031_112826_create_table_upload_image cannot be reverted.\n";

        return false;
    }

}
