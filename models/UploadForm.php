<?php
namespace app\models;

use yii\base\Model;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadForm extends ActiveRecord
{
    /**
     * @var UploadedFile|Null file attribute
     */
    public $file;

    public static function tableName()
    {
        return 'upload_image';
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['file'], 'file','extensions' => 'jpg, png', 'mimeTypes' => 'image/jpeg, image/png',],
        ];
    }
}
?>