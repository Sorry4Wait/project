<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Album';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-album">
    <?= Html::a('<i class="fa fa-upload"></i>Загрузить новый', ['image/index'], ['class' => 'btn btn-info']) ?>
        <div class="container" style="margin-top:40px;">
            <h2>Cписка фотографий</h2>
            <div class="demo-gallery">
                <ul id="lightgallery" class="list-unstyled row">
                    <?php foreach ($images as $img):?>
                        <li class="col-md-6">
                            <img id="<?=$img->id?>" class="img-responsive" src=<?=Yii::getAlias('@web').'/upload/'.$img->filename;?>>
                            <br />
                            <?= Html::a('Удалить', ['image/delete','id'=>$img->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Поворот', [''], ['class' => 'btn btn-warning','id' => 'rotate']) ?>
                            <br />
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
</div>

<?php
$this->registerCss('
.spinEffect{
   transform: rotate(180deg);
   -webkit-transform: rotate(180deg);
   -ms-transform: rotate(180deg);
}
');
$this->registerJs('
$(\'#lightgallery #rotate\').click(function(e){
    e.preventDefault();
    let degri = 0;
    let a = "#";
    let id = a + e.target.parentElement.childNodes[1].id;
    $(id).toggleClass("spinEffect");
    
 
});

');
?>