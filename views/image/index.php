<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = 'Index';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="image-index">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'file')->fileInput() ?>

    <?= Html::submitButton('Загрузить', ['class' => 'btn btn-success']) ?>
    <?= Html::a('Посмотреть Альбом', ['image/album'], ['class' => 'btn btn-info']) ?>
    <?php ActiveForm::end(); ?>
    <br />

</div>
